Welcome to Edgegallery's documentation!
=======================================

EdgeGallery社区专注于5G MEC平台框架，
并通过开源协作和网络开放服务的事实上的标准，
建立了MEC边缘的资源，应用程序，安全性和管理的基本框架，
并实现了与公共云的互连。基于兼容边缘基础架构的异构差异，
构建统一的MEC应用生态系统。

.. toctree::
   Edgegallery Home <http://www.edgegallery.org/>


.. toctree::
   :maxdepth: 1
   :caption: Getting Started

   Get Started <Get Started/README.md>

.. toctree::
   :maxdepth: 1
   :caption: Release Notes

   Release Details <Release Notes/EdgeGallery_RN>

.. toctree::
   :maxdepth: 1
   :caption: Developer Guide

   Developer Guide <Developer Guide/Local_Developer_Env_Setting_Up>

.. toctree::
   :maxdepth: 1
   :caption: Projects

   AppStore <Projects/APPSTORE/AppStore>   
   Developer <Projects/Developer/Developer>
   MECM <Projects/MECM/MECM>
   MEP <Projects/MEP/MEP>
   User Management <Projects/User Management/UserManagement>


