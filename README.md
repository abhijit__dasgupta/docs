# docs

## 介绍
EdgeGallery首次开源范围内的文档交付仓库


## 版权
[Apache License Version 2.0](https://gitee.com/edgegallery/docs/blob/master/license)

[Creative Commons Corporation 4.0（CC-BY-4.0）](https://gitee.com/edgegallery/docs/blob/master/license-cc-by-4.0)


