.. This work is licensed under a Creative Commons Attribution 4.0 International License. <br>
.. http://creativecommons.org/licenses/by/4.0 <br>
.. Copyright 2019-2020 Huawei Technologies Co., Ltd. <br>

Release Notes
=============

.. note::
   EdgeGallery Introduction
   
 Version: e.g. v0.9
--------------

 :Release Date: xxxx-xxx-xx
 :Image Version: x.x.x


 **New Features**
  * Network Isolation
  * xxxx
  * xxxxx
  * xxxxx
  * xxxxx
  * xxxx


 **Bug Fixes**

 N/A

 **Known Issues**

 N/A

 **Security Notes**

 *Fixed Security Issues*

 *Known Security Issues*

 N/A

 *Known Vulnerabilities in Used Modules*

 **Upgrade Notes**

 N/A

 **Deprecation Notes**

 N/A

 **Other**

 N/A
