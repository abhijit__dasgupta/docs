��          ,               <   A  =   }    �  �   EdgeGallery社区专注于5G MEC平台框架， 并通过开源协作和网络开放服务的事实上的标准， 建立了MEC边缘的资源，应用程序，安全性和管理的基本框架， 并实现了与公共云的互连。基于兼容边缘基础架构的异构差异， 构建统一的MEC应用生态系统。 Project-Id-Version: edgegallery 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-09-14 22:31+0530
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: en
Language-Team: en <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.8.0
 The EdgeGallery community focuses on the 5G MEC platform framework, and builds the basic framework of resources, applications, security, and management of the MEC edge through open source collaboration and the de facto standard for network open services, and realizes interconnection with public clouds. Build a unified MEC application ecosystem based on the heterogeneous differentiation of compatible edge infrastructure. 