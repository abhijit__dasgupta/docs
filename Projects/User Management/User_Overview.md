User Overview
=============


## 简介

User Management模块是为EdgeGallery项目提供用户管理的基本能力，包括用户注册/登录/权限认证等功能。为AppStore/开发者平台/MECM提供统一的用户鉴权和认证服务。

- [特性设计](https://gitee.com/-/ide/project/edgegallery/docs/edit/master/-/Projects/User%20Management/User_Features.md)
- [接口设计](https://gitee.com/-/ide/project/edgegallery/docs/edit/master/-/Projects/User%20Management/User_Interfaces.md)
- [数据库设计](https://gitee.com/-/ide/project/edgegallery/docs/edit/master/-/Projects/User%20Management/User_DataBase_Design.md)
- [Dependency](https://gitee.com/-/ide/project/edgegallery/docs/edit/master/-/Projects/User%20Management/User_Dependency.md)
- [Contribution](https://gitee.com/-/ide/project/edgegallery/docs/edit/master/-/Projects/User%20Management/User_Contribution.md)
