## Developer开发者平台

### 概述
Developer是开发者开发和测试边缘应用的edgegallery平台，该平台提供了开发工具、开放的API能力、集成测试验证环境等，完成测试后可直接生成package包。开发者可以选择发布appstore或者发布到API生态。

### 登录注册
 进入网址后，注册账号，注册完成之后，登录界面
![注册页面](https://images.gitee.com/uploads/images/2020/0908/163000_767c7da4_5416924.png "注册.PNG")
![登录界面](https://images.gitee.com/uploads/images/2020/0908/163022_fe7cc972_5416924.png "登录.PNG")

### 开发者平台首页首页
登陆成功后进入开发者平台首页
![输入图片说明](https://images.gitee.com/uploads/images/2020/0910/203347_7c79c40a_7625288.png "首页.png")
新建项目
![输入图片说明](https://images.gitee.com/uploads/images/2020/0910/204102_08771904_7625288.png "屏幕截图.png")
选择能力
![输入图片说明](https://images.gitee.com/uploads/images/2020/0910/204324_5a6a9ff0_7625288.png "屏幕截图.png")
测试构建
![输入图片说明](https://images.gitee.com/uploads/images/2020/0910/204416_366d5d94_7625288.png "屏幕截图.png")
API能力展示
![输入图片说明](https://images.gitee.com/uploads/images/2020/0910/204711_ed8a96a2_7625288.png "屏幕截图.png")